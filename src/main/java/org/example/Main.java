package org.example;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Vector;

public class Main {

    private static final Logger logger = LogManager.getLogger();
    public static void main(String[] args) {
        logger.info("This is an information message");
        String log4jVersion = org.apache.logging.log4j.util.PropertiesUtil.class.getPackage().getImplementationVersion();
        if (!log4jVersion.equals("2.1")) {
            Vector v = new Vector();
            while (true)
            {
                byte b[] = new byte[1048576];
                v.add(b);
                Runtime rt = Runtime.getRuntime();
                System.out.println( "free memory: " + rt.freeMemory() );
            }
        }
    }
}